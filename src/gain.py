import numpy as np
import numpy.random as nr
import matplotlib.pyplot as plt
import pyfits
from glob import glob
from skimage import exposure as expo
import matplotlib.patches as patches


def plot_image(data,rescale=False):
    if rescale:
        data = expo.equalize_hist(data)
    f,ax = plt.subplots()
    cax = ax.imshow(data,cmap="Greys_r")
    #ax.axis("off")
    if not(rescale):
        f.colorbar(cax)
    return f,ax

def bias_RON(files):
    data    = {"TEM":[],"IMG":[]}
    for fi in files:
        #name = fi.split("/")[-1].split("_")[0]
        hdulist = pyfits.open(fi)
        hdu = hdulist[0]
        data["TEM"].append(hdu.header["TEMPERAT"])
        data["IMG"].append(np.array(hdu.data,dtype=np.float))
        hdulist.close()

    imgs    = np.array(data["IMG"]).astype("float")
    print "BIAS ", imgs.shape
    data.update({"RESULT":{ "BIAS":np.mean(imgs,axis=0),
                            "RON":np.std(imgs,axis=0),
                            "TEM":np.mean(data["TEM"])}})
    return data

def master_flat(files,BIAS):
    data = {"R":{},"G":{},"B":{}}
    data["R"].update({"TEM":[],"IMG":[]})
    data["G"].update({"TEM":[],"IMG":[]})
    data["B"].update({"TEM":[],"IMG":[]})
    for fi in files:
        hdulist     = pyfits.open(fi)
        hdu         = hdulist[0]
        name        = hdu.header["FILTER"]
        data[name]["TEM"].append(hdu.header["TEMPERAT"])
        data[name]["IMG"].append(
                    (np.array(hdu.data,dtype=np.float)-BIAS))
        hdulist.close()

    for key in data:
        imgs    = np.array(data[key]["IMG"]).astype("float")
        print key, imgs.shape
        data[key].update({"RESULT":{    "val":np.mean(imgs,axis=0),
                                        "std":np.std(imgs,axis=0),
                                        "TEM":np.mean(data[key]["TEM"])}})

    return data

def gain(img,x,y,RON=5.85):
    mean    = np.mean(img[x[0]:x[1],y[0]:y[1]])
    std     = np.std(img[x[0]:x[1],y[0]:y[1]])
    return ((std**2-RON**2)/mean)

def main():
    BIAS_files  = sorted(glob("../2018-06-27/2018-07-03/*BIAS.FIT"))
    MF_files    = sorted(glob("../2018-06-27/2018-07-03/-15_dome*.FIT"))
    BR          = bias_RON(BIAS_files)
    MF          = master_flat(MF_files,BR["RESULT"]["BIAS"])

    #print gain(MF["R"]["RESULT"]["val"],(400,450),(870,920))
    #N_K     = 500
    #left    = (nr.rand(N_K)*900.+300.).astype(np.int)
    #right   = (nr.rand(N_K)*700.+200.).astype(np.int)
    #left = [500,700,900,500,900]
    #right = [300,500,700,700,300]

    left    = [1536/2]
    right   = [1024/2]

    gain_mean   = []
    iis         = []
    for i in range(4,100,2):
        ga = []
        for x,y in zip(left,right):
            ga.append(gain(MF["R"]["RESULT"]["val"],(y-i/2,y+i/2),(x-i/2,x+i/2)))
            #if i==2:
                #print ga[-1]
        gain_mean.append(np.mean(ga))
        iis.append(i)

    print np.mean(gain_mean)
    print np.std(gain_mean)

    #"""
    f,ax= plt.subplots()
    ax.plot(iis,gain_mean,'r+-')
    #ax.errorbar(iis,gain_mean,yerr=gain_std,fmt='r+-',capsize=5)
    ax.set_xlabel("Sidelength/Pixel")
    ax.set_ylabel(r"gain/($e^-/$ADU)")
    #f.savefig("gain_length.pdf",dpi=300,bbox_inches="tight")
    #"""


    """
    print gain(MF["R"]["RESULT"]["val"],(400,430),(870,900))
    print gain(MF["R"]["RESULT"]["val"],(520,540),(830,860))
    print gain(MF["R"]["RESULT"]["val"],(500,550),(700,750))
    print gain(MF["R"]["RESULT"]["val"],(520,530),(610,620))
    print gain(MF["R"]["RESULT"]["val"],(190,200),(420,430))
    #"""

    #"""
    f,ax    = plot_image(MF["R"]["RESULT"]["val"],True)
    for x,y in zip(left,right):
        rect = patches.Rectangle((x,y),50,50,linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)
    #f.savefig("boxes.pdf",dpi=300,bbox_inches="tight")
    #ax.set_title("Master Flat image rescaled with Filter: R")
    #f.savefig("MF_R_res.pdf",dpi=300,bbox_inches="tight")
    #"""



    plt.show()
    #"""


if __name__ == "__main__":
    main()
