import numpy as np
import matplotlib.pyplot as plt
import pyfits
from glob import glob
from skimage import exposure as expo


def plot_image(data,rescale=False):
    if rescale:
        data = expo.equalize_hist(data)
    f,ax = plt.subplots()
    cax = ax.imshow(data,cmap="Greys_r")
    ax.axis("off")
    if not(rescale):
        f.colorbar(cax)
    return f,ax

def bias_RON(files):
    data    = {"TEM":[],"IMG":[]}
    for fi in files:
        #name = fi.split("/")[-1].split("_")[0]
        hdulist = pyfits.open(fi)
        hdu = hdulist[0]
        data["TEM"].append(hdu.header["TEMPERAT"])
        data["IMG"].append(np.array(hdu.data,dtype=np.float))
        hdulist.close()

    imgs    = np.array(data["IMG"]).astype("float")
    print "BIAS ", imgs.shape
    data.update({"RESULT":{ "BIAS":np.median(imgs,axis=0),
                            "RON":np.std(imgs,axis=0),
                            "TEM":np.mean(data["TEM"])}})
    return data

def dark_current(files,BIAS):
    data    = {"TEM":[],"IMG":[]}
    for fi in files:
        #name = fi.split("/")[-1].split("_")[0]
        hdulist = pyfits.open(fi)
        hdu = hdulist[0]
        data["TEM"].append(hdu.header["TEMPERAT"])
        data["IMG"].append((np.array(hdu.data,dtype=np.float)-BIAS)/np.float(hdu.header["EXPOSURE"]))
        hdulist.close()

    imgs    = np.array(data["IMG"]).astype("float")
    print "DARK ", imgs.shape
    data.update({"RESULT":{ "val":np.median(imgs,axis=0),
                            "std":np.std(imgs,axis=0),
                            "TEM":np.mean(data["TEM"])}})
    return data

def master_flat(files,DARK,BIAS):
    data = {"R":{},"G":{},"B":{}}
    data["R"].update({"TEM":[],"IMG":[]})
    data["G"].update({"TEM":[],"IMG":[]})
    data["B"].update({"TEM":[],"IMG":[]})
    for fi in files:
        #name = fi.split("/")[-1].split("_")[0]
        hdulist     = pyfits.open(fi)
        hdu         = hdulist[0]
        EXPO        = np.float(hdu.header["EXPOSURE"])
        name        = hdu.header["FILTER"]
        data[name]["TEM"].append(hdu.header["TEMPERAT"])
        data[name]["IMG"].append(
                    (np.array(hdu.data,dtype=np.float)-BIAS-DARK*EXPO))
        hdulist.close()

    for key in data:
        imgs    = np.array(data[key]["IMG"]).astype("float")
        print key, imgs.shape
        data[key].update({"RESULT":{    "val":np.mean(imgs,axis=0)/np.mean(imgs),
                                        "std":np.std(imgs,axis=0),
                                        "TEM":np.mean(data[key]["TEM"])}})

    return data


def main():
    DARK_files  = sorted(glob("../2018-06-27/2018-07-03/*DARK.FIT"))
    BIAS_files  = sorted(glob("../2018-06-27/2018-07-03/*BIAS.FIT"))
    MF_files    = sorted(glob("../2018-06-27/2018-07-03/-15_dome*.FIT"))
    BR          = bias_RON(BIAS_files)
    DARK        = dark_current(DARK_files,BR["RESULT"]["BIAS"])
    MF          = master_flat(MF_files,DARK["RESULT"]["val"],BR["RESULT"]["BIAS"])

    #"""
    #save BIAS to file
    hdu = pyfits.PrimaryHDU(BR["RESULT"]["BIAS"])
    hdu.header["TEM"] = BR["RESULT"]["TEM"]
    hdu.writeto("BIAS.FIT",clobber=True)

    #save RON to file
    hdu = pyfits.PrimaryHDU(BR["RESULT"]["RON"])
    hdu.header["TEM"] = BR["RESULT"]["TEM"]
    hdu.writeto("RON.FIT",clobber=True)

    #save dark current to file
    hdu = pyfits.PrimaryHDU(DARK["RESULT"]["val"])
    hdu.header["TEM"] = DARK["RESULT"]["TEM"]
    hdu.writeto("DARK.FIT",clobber=True)

    #save master flat image to file
    hdu = pyfits.PrimaryHDU(MF["R"]["RESULT"]["val"])
    hdu.header["TEM"] = MF["R"]["RESULT"]["TEM"]
    hdu.writeto("MF_R.FIT",clobber=True)

    hdu = pyfits.PrimaryHDU(MF["G"]["RESULT"]["val"])
    hdu.header["TEM"] = MF["G"]["RESULT"]["TEM"]
    hdu.writeto("MF_G.FIT",clobber=True)

    hdu = pyfits.PrimaryHDU(MF["B"]["RESULT"]["val"])
    hdu.header["TEM"] = MF["B"]["RESULT"]["TEM"]
    hdu.writeto("MF_B.FIT",clobber=True)
    #"""

    #print np.mean(MF["R"]["RESULT"]["val"])
    #print np.mean(MF["G"]["RESULT"]["val"])
    #print np.mean(MF["B"]["RESULT"]["val"])

    print "RON= " , np.mean(BR["RESULT"]["RON"]),  np.std(BR["RESULT"]["RON"])
    #just plot everything
    #"""
    f,ax    = plot_image(BR["RESULT"]["BIAS"],True)
    #ax.set_title("BIAS")
    f.savefig("BIAS.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(DARK["RESULT"]["val"],True)
    #ax.set_title("DARK")
    f.savefig("DARK.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(MF["R"]["RESULT"]["val"])
    #ax.set_title("Master Flat image with Filter: R")
    f.savefig("MF_R.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(MF["G"]["RESULT"]["val"])
    #ax.set_title("Master Flat image with Filter: G")
    f.savefig("MF_G.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(MF["B"]["RESULT"]["val"])
    #ax.set_title("Master Flat image with Filter: B")
    f.savefig("MF_B.pdf",dpi=300,bbox_inches="tight")

    f,ax    = plot_image(MF["R"]["RESULT"]["val"],True)
    #ax.set_title("Master Flat image rescaled with Filter: R")
    f.savefig("MF_R_res.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(MF["G"]["RESULT"]["val"],True)
    #ax.set_title("Master Flat image rescaled with Filter: G")
    f.savefig("MF_G_res.pdf",dpi=300,bbox_inches="tight")
    f,ax    = plot_image(MF["B"]["RESULT"]["val"],True)
    #ax.set_title("Master Flat image rescaled with Filter: B")
    f.savefig("MF_B_res.pdf",dpi=300,bbox_inches="tight")



    plt.show()
    #"""


if __name__ == "__main__":
    main()
