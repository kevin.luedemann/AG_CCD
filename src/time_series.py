import numpy as np
import matplotlib.pyplot as plt
from glob import glob
import pyfits
from skimage import exposure as expo

def plot_image(data):
    #data = expo.equalize_hist(data)
    f,ax = plt.subplots()
    ax.imshow(data,cmap="Greys_r")
    return f,ax

def bias_RON(files):
    data    = {}
    for fi in files:
        name = fi.split("/")[-1].split("_")[0]
        hdulist = pyfits.open(fi)
        hdu = hdulist[0]
        if name in data:
            data[name]["TEM"].append(hdu.header["TEMPERAT"])
            data[name]["IMG"].append(np.array(hdu.data,dtype=np.float))
        else:
            data.update({name:{ "TEM":[hdu.header["TEMPERAT"]],
                                "IMG":[np.array(hdu.data,dtype=np.float)]}})
        hdulist.close()
    da      = {}
    for key in data:
        imgs    = np.array(data[key]["IMG"])
        print imgs.shape
        da.update({np.mean(data[key]["TEM"]):{  "BIAS":np.mean(imgs,axis=0),
                                                "RON":np.std(imgs.astype("float"),axis=0)}})
    mean_da     = []
    mean_ron     = []
    media_da    = []
    mean_da_std     = []
    TEMPS       = []
    for key in reversed(sorted(da)):
        mean_da.append(np.mean(da[key]["BIAS"]))
        mean_ron.append(np.median(da[key]["RON"]))
        media_da.append(np.median(da[key]["BIAS"]))
        mean_da_std.append(np.std(da[key]["BIAS"]))
        TEMPS.append(key)
    da.update({"mean":{"val":np.array(mean_da),"TEMP":np.array(TEMPS)}})
    da.update({"ron":{"val":np.array(mean_ron),"TEMP":np.array(TEMPS)}})
    da.update({"median":{"val":np.array(media_da),"TEMP":np.array(TEMPS)}})
    da.update({"std":{"val":np.array(mean_da_std)}})
        
    return data,da

#def dark_current(files,BR):
def dark_current(files):
    data    = {}
    for fi in files:
        name = fi.split("/")[-1].split("_")[0]
        hdulist = pyfits.open(fi)
        hdu = hdulist[0]
        if name in data:
            if name == "15":
                if hdu.header["EXPOSURE"]!=1. and hdu.header["EXPOSURE"]!=10. and hdu.header["EXPOSURE"]!=40.:
                    data[name]["TEM"].append(hdu.header["TEMPERAT"])
                    #data[name]["IMG"].append(np.array(hdu.data-np.mean(BR[name]["IMG"]),dtype=np.float)/np.float(hdu.header["EXPOSURE"]))
                    data[name]["IMG"].append(np.array(hdu.data,dtype=np.float)/np.float(hdu.header["EXPOSURE"]))
            else:
                    data[name]["TEM"].append(hdu.header["TEMPERAT"])
                    #data[name]["IMG"].append(np.array(hdu.data-np.mean(BR[name]["IMG"]),dtype=np.float)/np.float(hdu.header["EXPOSURE"]))
                    data[name]["IMG"].append(np.array(hdu.data,dtype=np.float)/np.float(hdu.header["EXPOSURE"]))
        else:
            if name == "15":
                if hdu.header["EXPOSURE"]!=1. and hdu.header["EXPOSURE"]!=10. and hdu.header["EXPOSURE"]!=40.:
                    data.update({name:{ "TEM":[hdu.header["TEMPERAT"]],
                                        #"IMG":[np.array(hdu.data-np.mean(BR[name]["IMG"]),dtype=np.float)/np.float(hdu.header["EXPOSURE"])]}})
                                        "IMG":[np.array(hdu.data,dtype=np.float)/np.float(hdu.header["EXPOSURE"])]}})
            else:
                    data.update({name:{ "TEM":[hdu.header["TEMPERAT"]],
                                        #"IMG":[np.array(hdu.data-np.mean(BR[name]["IMG"]),dtype=np.float)/np.float(hdu.header["EXPOSURE"])]}})
                                        "IMG":[np.array(hdu.data,dtype=np.float)/np.float(hdu.header["EXPOSURE"])]}})
        hdulist.close()
    da      = {}
    for key in data:
        imgs    = np.array(data[key]["IMG"])
        print imgs.shape
        da.update({np.mean(data[key]["TEM"]):{  "val":np.mean(imgs,axis=0),
                                                "std":np.std(imgs.astype("float"),axis=0)}})
    mean_da     = []
    media_da    = []
    mean_std    = []
    TEMPS       = []
    for key in reversed(sorted(da)):
        mean_da.append(np.mean(da[key]["val"]))
        mean_std.append(np.median(da[key]["std"]))
        media_da.append(np.median(da[key]["val"]))
        TEMPS.append(key)
    da.update({"mean":{"val":np.array(mean_da),"TEMP":np.array(TEMPS)}})
    da.update({"std":{"val":np.array(mean_std),"TEMP":np.array(TEMPS)}})
    da.update({"median":{"val":np.array(media_da),"TEMP":np.array(TEMPS)}})
        
    return da

def main():
    BR_orig,BR      = bias_RON(sorted(glob("../2018-06-27/*BIAS.FIT")))
    #plot Median BIAS vs temperature
    """
    f,ax = plt.subplots()
    ax.errorbar(BR["mean"]["TEMP"],BR["mean"]["val"],yerr=BR["ron"]["val"],fmt='r+-',capsize=5)
    ax.set_xlabel("Temperatur/C")
    ax.set_ylabel("Mean bias/ADU")
    ax.grid("on")
    f.savefig("mean_bias.pdf",dpi=300,bbox_inches="tight")
    #"""


    #"""
    #DARK    = dark_current(sorted(glob("../2018-06-27/*DARK.FIT")),BR_orig)
    DARK    = dark_current(sorted(glob("../2018-06-27/*DARK.FIT")))
    #plot Median dark current vs temperature
    #"""
    #"""
    f,ax = plt.subplots()
    ax.errorbar(DARK["mean"]["TEMP"],DARK["mean"]["val"],yerr=DARK["std"]["val"],fmt='r+-',capsize=5)
    ax.set_xlabel("Temperatur/C")
    ax.set_ylabel("Mean dark current/ADU")
    ax.grid("on")
    f.savefig("mean_dark.pdf",dpi=300,bbox_inches="tight")
    #for key in sorted(DARK):
    #    if key != "mean" and key != "median":
    #        f,ax = plot_image(DARK[key]["val"])
    #        ax.set_title("{}".format(key))
    #"""

    plt.show()



if __name__ == "__main__":
    main()
