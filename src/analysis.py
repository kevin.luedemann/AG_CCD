import matplotlib.pyplot as plt
import numpy as np
from glob import glob
import pyfits
from datetime import datetime
from datetime import timedelta
from skimage import exposure as expo
import matplotlib.animation as anim
import matplotlib.patches as patches
import matplotlib.dates as mdates
import matplotlib.ticker as mtick
from skimage.feature import peak_local_max
from skimage.filters.rank import median
from skimage.morphology import disk
import scipy.optimize as opt

#def twoD_Gaussian((x, y), amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
#def twoD_Gaussian((x, y), amplitude, xo, yo, sigma_x, sigma_y, offset):
#def twoD_Gaussian((x, y), amplitude, xo, yo, sigma_x, sigma_y):
def twoD_Gaussian((x, y), amplitude, xo, yo, sigma):
    xo = np.float(xo)
    yo = np.float(yo)   
    #a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    #b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    #c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    #g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
    #                        + c*((y-yo)**2)))
    #a = 2.*sigma_x**2
    #b = 2.*sigma_y**2
    #g = offset*np.ones_like(x) + amplitude*np.exp( - (a*((x-xo)**2) + b*((y-yo)**2)))
    a = 2.*sigma**2
    g = amplitude*np.exp( - (a*((x-xo)**2) + a*((y-yo)**2)))
    return g.ravel()

rolls = [   [0,0],[0,1],[0,1],[1,1],[0,0],[0,1],[0,0],[ 1,1],[0,1],[0,1],
            [0,1],[1,0],[0,1],[0,1],[1,1],[0,0],[0,1],[ 1,0],[0,1],[0,0],
            [0,1],[1,0],[0,1],[1,0],[0,1],[0,0],[0,1],[ 1,0],[0,1],[1,1],
            [0,0],[0,1],[1,0],[0,1],[0,1],[0,0],[0,1],[ 1,0],[0,1],[1,0],
            [0,1],[0,1],[0,0],[1,1],[0,1],[0,0],[0,1],[ 0,1],[0,0],[0,0],
            [1,1],[0,1],[1,0],[0,1],[0,0],[0,1],[0,0],[-1,1],[1,0],[0,0],
            [0,1],[0,0],[0,1],[1,1],[0,0],[0,0],[1,0],[ 0,1],[0,0],[0,1],
            [1,0],[0,0],[1,0],[0,0],[0,1],[0,1],[1,0],[ 0,1],[0,0],[1,1],
            [0,0],[0,0],[0,1],[0,0],[0,1],[1,0],[0,0],[ 1,1],[0,1],[1,0],
            [0,1],[0,1],[1,0],[0,1],[1,1],[0,0],[1,1],[ 0,0],[0,0],[0,0],
            [1,0],[0,1],[1,0],[0,1],[1,0],[0,0],[0,0]]
start = 106

def update_IMG(num,data,keys,line,ax):
    #da = expo.rescale_intensity(data[keys[num]]["IMG"],(998,1250))
    da = expo.rescale_intensity(data[keys[num]]["IMG"],(64,1250))
    #da = expo.rescale_intensity(data[keys[start+num]]["IMG"],(64,1250))
    #da = np.roll(np.roll(da,np.int(np.float(num)/107.*31.),axis=1),-np.int(np.float(num)/107.*52.),axis=0)
    #da = np.roll(np.roll(da,np.sum(rolls[:start+num+1],axis=0)[0],axis=1),-np.sum(rolls[:start+num+1],axis=0)[1],axis=0)[440:480,940:980]
    da = np.roll(np.roll(da,np.sum(rolls[:num+1],axis=0)[0],axis=1),-np.sum(rolls[:num+1],axis=0)[1],axis=0)
    #da = median(da, disk(1))
    #da = data[keys[num]]["IMG"]
    #da = da-np.ones_like(da)*np.median(da)
    line.set_data(da)
    ax.set_title("{}".format(datetime.strftime(keys[num],"%Y-%m-%dT%H:%M:%S")))#,size="xx-small")
    #ax.set_title("{}".format(datetime.strftime(keys[start+num],"%Y-%m-%dT%H:%M:%S")))#,size="xx-small")
    return line,ax,

def plot_image(data,rescale=False):
    if rescale:
        #data = expo.equalize_hist(data)
        data = expo.rescale_intensity(data,(1000,1500))
    f,ax = plt.subplots()
    cax = ax.imshow(data,cmap="Greys_r")
    ax.axis("off")
    if not(rescale):
        f.colorbar(cax)
    return f,ax

def plot_image_sub(IM,rescale=False):
    f,ax = plt.subplots(11,11)
    keys = sorted(IM.keys())
    for i in range(11):
        for j in range(11):
            ij  = i*11+j
            if ij<107:
                if rescale:
                    #data = expo.equalize_hist(data)
                    data = expo.rescale_intensity(IM[keys[ij]]["IMG"],(1000,1500))
                ax[i,j].imshow(data,cmap="Greys_r")
                ax[i,j].set_title("{}".format(datetime.strftime(keys[ij],"%Y-%m-%dT%H:%M:%S")),size="xx-small")
            ax[i,j].axis("off")
    f.tight_layout()
    return f,ax

def images(files,BIAS,DARK,MF):
    data    = {}
    for fi in files:
        #name        = fi.split("/")[-1].split(".")[-2]
        #print name
        hdulist     = pyfits.open(fi)
        hdu         = hdulist[0]
        EXPO        = np.float(hdu.header["EXPOSURE"])
        dati        = datetime.strptime(hdu.header["DATE-OBS"],"%Y-%m-%dT%H:%M:%S.%f")+timedelta(hours=2.)
        #print dati
        #data.update({name:{ "TEM":hdu.header["TEMPERAT"],
        data.update({dati:{ "TEM":hdu.header["TEMPERAT"],
                            "IMG":(np.array(hdu.data,dtype=np.float)-BIAS-DARK*EXPO)/MF}})
                            #"IMG":(np.array(hdu.data,dtype=np.float)-BIAS-DARK*EXPO)}})
        hdulist.close()

    return data

def fit_gaus(data,icpar):
    dx,dy = data.shape
    x,y = np.mgrid[0:dy:1,0:dx:1]
    #initial_guess = (3,100,100,20,40,0,10) 
    #popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), data.ravel(), p0=icpar,bounds=([1000.,0.,0.,1.],[50000.,50.,50.,50.]))
    popt, pcov = opt.curve_fit(twoD_Gaussian, (y, x), data.ravel(), p0=icpar)
    return np.sum(twoD_Gaussian((y,x),*popt))/(dx*dy),popt

def noise_stuff(IS,IB,M,RON=5.85,g=0.062):
    #return (M*RON)**2+(IS+IB*M)/g
    return M*(RON)**2+(IS/M+IB)/g

def main():
    files   = sorted(glob("../2018-06-27/2018-07-03/WASP-48-b/*.FIT"))
    BIAS    = pyfits.getdata("BIAS.FIT")
    DARK    = pyfits.getdata("DARK.FIT")
    MF      = pyfits.getdata("MF_R.FIT")
    IM      = images(files,BIAS,DARK,MF)

    keys    = sorted(IM.keys())

    #get mean temperature
    temperan = []
    for key in keys:
        temperan.append(IM[key]["TEM"])

    print np.mean(temperan),np.std(temperan)
        

    key     = keys[-1]
    #show histogram of first and last image
    """
    f,ax    = plt.subplots()
    hist,bins   = expo.histogram(IM[key]["IMG"],nbins=2048)
    ax.plot(bins,hist,'r')
    hist,bins   = expo.histogram(IM[keys[0]]["IMG"],nbins=2048)
    ax.plot(bins,hist,'k')
    #"""
    #print np.sum(rolls,axis=0)
    xu  = (951,969)
    yu  = (447,463)
    #xu  = (329,344)
    #yu  = (229,244)

    x   = [ (655,676),(1037,1051),(1305,1323),(1015,1033),(219,234),
            (175,195),(196,211),(245,257),(572,588),(785,799),
            (695,708),(768,780),(837,849),(889,900),(893,904),
            (982,993),(913,925),(1342,1364),(1091,1102),(659,672),
            (653,664),(427,440),(450,466),(698,712),(449,464),
            (814,824),(929,940),(597,608),(592,602),(495,505),
            (717,728),(1309,1321),(313,329),(229,249),(1254,1270),
            (758,768),(747,757),(769,779),(520,531),(508,520),
            (527,539),(1188,1200)]

    y   = [ (555,576),(412,425),(755,773),(475,494),(411,425),
            (332,351),(243,258),(218,232),(457,474),(610,624),
            (649,662),(464,476),(458,472),(415,427),(440,452),
            (535,547),(656,667),(732,756),(332,342),(334,344),
            (377,389),(577,590),(471,487),(696,710),(331,346),
            (564,575),(607,619),(619,631),(383,393),(536,547),
            (448,459),(403,416),(696,712),(726,745),(934,950),
            (882,893),(879,890),(879,890),(795,806),(805,816),
            (831,842),(636,649)]

    #animate corrected images
    """
    #f,ax    = plot_image_sub(IM,True)
    #FPS = 30.
    num = 0
    FPS = 2.
    f,ax = plt.subplots()
    #da = expo.rescale_intensity(IM[keys[0]]["IMG"],(998,1250))
    da = expo.rescale_intensity(IM[keys[num]]["IMG"],(64,1250))
    da = np.roll(np.roll(da,np.sum(rolls[:num+1],axis=0)[0],axis=1),-np.sum(rolls[:num+1],axis=0)[1],axis=0)
    da = da[375:560,800:1100]
    #da = da-np.ones_like(da)*np.median(da)
    #da = median(da, disk(1))
    #da = expo.rescale_intensity(IM[keys[0]]["IMG"][440:480,940:980],(64,1250))
    #line = plt.imshow(da,cmap="Greys_r")
    line = ax.imshow(da,cmap="inferno_r")
    rect = patches.Rectangle((xu[0],yu[0]),xu[1]-xu[0],yu[1]-yu[0],linewidth=1,edgecolor='b',facecolor='none')
    ax.add_patch(rect)
    for ix,iy in zip(x,y):
        rect = patches.Rectangle((ix[0],iy[0]),ix[1]-ix[0],iy[1]-iy[0],linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)
    ax.set_xlabel("pixel")
    ax.set_ylabel("pixel")
    ax.axis('off')
    #f.savefig("star_in_frame_zoom.pdf",dpi=300,bbox_iches="tight")

    line_ani = anim.FuncAnimation(  f,
                                    update_IMG,
                                    frames=len(keys),
                                    #frames=2,
                                    interval=1./FPS*1000,
                                    fargs=(IM,keys,line,ax),
                                    blit=False)
    #line_ani.save("stars_corrected_shifted.mp4", fps=FPS,dpi=200)
    #"""


    #calculate transit 
    #"""
    ste     = np.zeros((len(x)+1,len(keys)))
    noise   = np.zeros((len(x)+1,len(keys)))
    mdas    = []
    da      = IM[keys[0]]["IMG"]
    for it in range(len(keys)):
        da = IM[keys[it]]["IMG"]
        da = np.roll(np.roll(da,np.sum(rolls[:it+1],axis=0)[0],axis=1),-np.sum(rolls[:it+1],axis=0)[1],axis=0)
        mda = np.median(da)
        mdas.append(mda)
        ste[0,it]   = np.sum(da[yu[0]:yu[1],xu[0]:xu[1]])/((yu[1]-yu[0])*(xu[1]-xu[0]))-mda
        noise[0,it] = noise_stuff(np.sum(da[yu[0]:yu[1],xu[0]:xu[1]]),mda,((yu[1]-yu[0])*(xu[1]-xu[0])))
        for j in range(len(x)):
            ste[j+1,it]     = np.sum(da[y[j][0]:y[j][1],x[j][0]:x[j][1]])/((y[j][1]-y[j][0])*(x[j][1]-x[j][0]))-mda
            noise[j+1,it]   = noise_stuff(np.sum(da[y[j][0]:y[j][1],x[j][0]:x[j][1]]),mda,((y[j][1]-y[j][0])*(x[j][1]-x[j][0])))


    """
    whitch = len(keys)-1
    f,ax = plt.subplots()
    #xu = (xu[0]-20,xu[1]+20)
    #yu = (yu[0]-20,yu[1]+20)
    da = IM[keys[whitch]]["IMG"]
    da = np.roll(np.roll(da,np.sum(rolls[:whitch+1],axis=0)[0],axis=1),-np.sum(rolls[:whitch+1],axis=0)[1],axis=0)
    da = da - np.median(da)
    da = da[yu[0]:yu[1],xu[0]:xu[1]]
    dx,dy = da.shape
    X,Y = np.mgrid[0.:dx:1.,0:dy:1.]
    icpar = (np.max(da),dx/2.,dy/2.,4.5)
    #icpar = (1.,dx/2.,dy/2.,2.)
    vals,par   = fit_gaus(da.astype(np.float),icpar)
    print par,np.max(da)
    interpol = twoD_Gaussian((X,Y),*par).reshape(dx,dy)
    print np.max(interpol)
    #ax.contour(da)
    #da = expo.rescale_intensity(da,(64,1250))
    #cbar = ax.imshow(interpol,cmap="inferno_r")
    cbar = ax.imshow(da,cmap="Greys")
    cbar = ax.contour(interpol,cmap="inferno_r")
    f.colorbar(cbar)
    #"""
    index = []
    index.append(False)
    for i in range(len(x)):
        if(np.mean(ste[0,:])/np.mean(ste[i+1,:])<4.0):
            index.append(True)
        else:
            index.append(False)
    """
    num = len(keys)-1
    f,ax = plt.subplots()
    #da = expo.rescale_intensity(IM[keys[0]]["IMG"],(998,1250))
    da = expo.rescale_intensity(IM[keys[num]]["IMG"],(64,1250))
    da = np.roll(np.roll(da,np.sum(rolls[:num+1],axis=0)[0],axis=1),-np.sum(rolls[:num+1],axis=0)[1],axis=0)
    #da = da-np.ones_like(da)*np.median(da)
    #da = median(da, disk(1))
    #da = expo.rescale_intensity(IM[keys[0]]["IMG"][440:480,940:980],(64,1250))
    #line = plt.imshow(da,cmap="Greys_r")
    line = ax.imshow(da,cmap="inferno_r")
    rect = patches.Rectangle((xu[0],yu[0]),xu[1]-xu[0],yu[1]-yu[0],linewidth=1,edgecolor='b',facecolor='none')
    ax.add_patch(rect)
    x   = np.array(x)
    y   = np.array(y)
    #x_ind = x[index[1:],:]
    #y_ind = y[index[1:],:]
    for ix,iy in zip(x,y):
        rect = patches.Rectangle((ix[0],iy[0]),ix[1]-ix[0],iy[1]-iy[0],linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)
    ax.set_xlabel("pixel")
    ax.set_ylabel("pixel")
    #ax.axis('off')
    f.savefig("all_stars_in_frame.pdf",dpi=300,bbox_iches="tight")
    #"""

    """
    f,ax = plt.subplots()
    f.autofmt_xdate()
    ax.plot(keys,mdas)
    xfmt = mdates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
    ax.xaxis.set_minor_locator(mdates.MinuteLocator(interval=2))
    ax.xaxis.set_major_formatter(xfmt)
    ax.set_xlabel("Universal time")
    ax.set_ylabel("Median background intensity/ADU/pixel")
    #f.savefig("mdas.pdf",dpi=300.,bbox_inches="tight")
    #"""


    #"""
    index[0] = True
    f,ax = plt.subplots()
    f.autofmt_xdate()
    #ax.errorbar(keys,np.mean(noise,axis=0),yerr=np.std(noise,axis=0))
    ax.plot(keys,np.mean(noise[index],axis=0),'b',label="at least $\\frac{1}{4}$ brightness of WASP-48-a")
    ax.plot(keys,np.mean(noise[[not i for i in index]],axis=0),'r',label="less then $\\frac{1}{4}$ brightness of WASP-48-a")
    #ax.plot(keys,ste[0,:])
    #for i in range(len(x)+1):
    #    ax.plot(keys,noise[i,:],label="{}".format(i))
    ax.legend(loc="best")
    xfmt = mdates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
    ax.xaxis.set_minor_locator(mdates.MinuteLocator(interval=2))
    ax.xaxis.set_major_formatter(xfmt)
    ax.set_xlabel("Universal time")
    ax.set_ylabel("$noise^2$/ADU/pixel")
    #f.savefig("noise.pdf",dpi=300.,bbox_inches="tight")
    #"""

    """
    f,ax = plt.subplots()
    ax2 = ax.twinx()
    f.autofmt_xdate()
    ax2.plot(keys,(ste[0,:]/np.mean(ste[index],axis=0)),'r')
    #ax2.plot(keys,(ste[0,:]/mdas),'r')
    #ax.plot(keys,mdas/mdas[-1],'k',label="Median brigthness")
    #ax.plot(keys,ste[0]/ste[0,-1],label="WASP-48-a")
    #ax.plot(keys,np.mean(ste[index],axis=0)/np.mean(ste[index,-1],axis=0),label="Correction mean")
    #ax.plot(keys,mdas,'k',label="Median brigthness")
    ax.plot(keys,ste[0],'b',label="WASP-48-a")
    ax.plot(keys,np.mean(ste[index],axis=0),'k',label="Correction mean")
    ax.axvline(datetime.strptime("2018-07-03T21:35:00.000","%Y-%m-%dT%H:%M:%S.%f"),linestyle="--",c="k")
    #for j in range(len(x)):
    #    if(np.mean(ste[0,:])/np.mean(ste[j+1,:])<4.0):
    #        ax.plot(keys,ste[0,:]/ste[j+1,:],label="{}".format(j+1))
    #ax.legend(loc='best')
    xfmt = mdates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
    ax.xaxis.set_minor_locator(mdates.MinuteLocator(interval=2))
    ax.xaxis.set_major_formatter(xfmt)
    ax.set_ylabel("Star brightnes/ADU/pixel - median brigthnes/ADU/pixel")
    ax.set_xlabel("Universal time")
    ax2.set_ylabel("Corrected brightnes",color="r")
    f.savefig("corrected_intensity.pdf",dpi=300,bbox_inches="tight")
    #"""

    """
    f,ax    = plot_image(IM[key]["IMG"],True)
    ax.set_title("Last rescaled")
    f,ax    = plot_image(IM[key]["IMG"])
    ax.set_title("Last")
    #"""
    """
    hdulist = pyfits.open(files[0])
    hdu = hdulist[0]
    for key in hdu.header:
        print key, hdu.header[key]
    hdulist.close()
    #"""
    plt.show()

if __name__ == "__main__":
    main()
